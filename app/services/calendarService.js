managerApp.service('calendarService', function () {

    this.getActivity = function (id) {
        for (var i = 0; i < activities.length; i++) {
            if (activities[i].id === id) {
                return activities[i];
            }
        }
        return null;
    };
    
    this.getActivities = function(){
        return activities;
    }

    var activities = [
        { id: 1, enable: 1, dayofweek: 1, coach: 'Coach 01', hour: '09:00', type: 2, name: 'TBC' },
        { id: 2, enable: 1, dayofweek: 1, coach: 'Coach 01', hour: '10:00', type: 3, name: 'Pilates'},
        { id: 3, enable: 1, dayofweek: 1, coach: 'Coach 01', hour: '11:00', type: 4, name: 'Core'},
        { id: 4, enable: 1, dayofweek: 1, coach: 'Coach 02', hour: '15:30', type: 5, name: 'Crossfit'},
        { id: 5, enable: 1, dayofweek: 1, coach: 'Coach 03', hour: '16:30', type: 6, name: 'ABT'},
        { id: 6, enable: 1, dayofweek: 1, coach: 'Coach 04', hour: '17:00', type: 7, name: 'Power Bike'},
        { id: 7, enable: 1, dayofweek: 1, coach: 'Coach 05', hour: '18:00', type: 8, name: 'Zumba'},
        { id: 8, enable: 1, dayofweek: 1, coach: 'Coach 04', hour: '19:00', type: 7, name: 'Power Bike'},
        { id: 9, enable: 1, dayofweek: 1, coach: 'Coach 03', hour: '21:30', type: 9, name: 'Fat Burning'},
        
        { id: 10, enable: 1, dayofweek: 2, coach: 'Coach 01', hour: '09:00', type: 4, name: 'Core'},
        { id: 11, enable: 1, dayofweek: 2, coach: 'Coach 01', hour: '10:00', type: 3, name: 'Pilates'},
        { id: 12, enable: 1, dayofweek: 2, coach: 'Coach 01', hour: '11:00', type: 2, name: 'TBC' },
        { id: 13, enable: 1, dayofweek: 2, coach: 'Coach 04', hour: '17:00', type: 7, name: 'Power Bike'},
        { id: 14, enable: 1, dayofweek: 2, coach: 'Coach 03', hour: '18:30', type: 9, name: 'Fat Burning'},
        { id: 15, enable: 1, dayofweek: 2, coach: 'Coach 02', hour: '19:30', type: 5, name: 'Crossfit'},
        { id: 16, enable: 1, dayofweek: 2, coach: 'Coach 03', hour: '20:30', type: 6, name: 'ABT'},
        { id: 17, enable: 1, dayofweek: 2, coach: 'Coach 04', hour: '21:00', type: 7, name: 'Power Bike'},
        { id: 18, enable: 1, dayofweek: 2, coach: 'Coach 05', hour: '22:00', type: 8, name: 'Zumba'},
        
        { id: 21, enable: 1, dayofweek: 3, coach: 'Coach 01', hour: '09:00', type: 2, name: 'TBC' },
        { id: 22, enable: 1, dayofweek: 3, coach: 'Coach 01', hour: '10:00', type: 3, name: 'Pilates'},
        { id: 23, enable: 1, dayofweek: 3, coach: 'Coach 01', hour: '11:00', type: 4, name: 'Core'},
        { id: 24, enable: 1, dayofweek: 3, coach: 'Coach 02', hour: '15:30', type: 5, name: 'Crossfit'},
        { id: 25, enable: 1, dayofweek: 3, coach: 'Coach 03', hour: '16:30', type: 6, name: 'ABT'},
        { id: 26, enable: 1, dayofweek: 3, coach: 'Coach 04', hour: '17:00', type: 7, name: 'Power Bike'},
        { id: 27, enable: 1, dayofweek: 3, coach: 'Coach 05', hour: '18:00', type: 8, name: 'Zumba'},
        { id: 28, enable: 1, dayofweek: 3, coach: 'Coach 04', hour: '19:00', type: 7, name: 'Power Bike'},
        { id: 29, enable: 1, dayofweek: 3, coach: 'Coach 03', hour: '21:30', type: 9, name: 'Fat Burning'},
        
        { id: 30, enable: 1, dayofweek: 4, coach: 'Coach 01', hour: '09:00', type: 4, name: 'Core'},
        { id: 31, enable: 1, dayofweek: 4, coach: 'Coach 01', hour: '10:00', type: 3, name: 'Pilates'},
        { id: 32, enable: 1, dayofweek: 4, coach: 'Coach 01', hour: '11:00', type: 2, name: 'TBC' },
        { id: 33, enable: 1, dayofweek: 4, coach: 'Coach 04', hour: '17:00', type: 7, name: 'Power Bike'},
        { id: 34, enable: 1, dayofweek: 4, coach: 'Coach 03', hour: '18:30', type: 9, name: 'Fat Burning'},
        { id: 35, enable: 1, dayofweek: 4, coach: 'Coach 02', hour: '19:30', type: 5, name: 'Crossfit'},
        { id: 36, enable: 1, dayofweek: 4, coach: 'Coach 03', hour: '20:30', type: 6, name: 'ABT'},
        { id: 37, enable: 1, dayofweek: 4, coach: 'Coach 04', hour: '21:00', type: 7, name: 'Power Bike'},
        { id: 38, enable: 1, dayofweek: 4, coach: 'Coach 05', hour: '22:00', type: 8, name: 'Zumba'},
        
        { id: 40, enable: 1, dayofweek: 5, coach: 'Coach 01', hour: '09:00', type: 4, name: 'Core'},
        { id: 41, enable: 1, dayofweek: 5, coach: 'Coach 01', hour: '10:00', type: 3, name: 'Pilates'},
        { id: 42, enable: 1, dayofweek: 5, coach: 'Coach 01', hour: '11:00', type: 2, name: 'TBC' },
        { id: 43, enable: 1, dayofweek: 5, coach: 'Coach 04', hour: '17:00', type: 7, name: 'Power Bike'},
        { id: 44, enable: 1, dayofweek: 5, coach: 'Coach 03', hour: '18:30', type: 9, name: 'Fat Burning'},
        { id: 45, enable: 1, dayofweek: 5, coach: 'Coach 02', hour: '19:30', type: 5, name: 'Crossfit'},
        { id: 46, enable: 1, dayofweek: 5, coach: 'Coach 03', hour: '20:30', type: 6, name: 'ABT'},
        { id: 47, enable: 1, dayofweek: 5, coach: 'Coach 04', hour: '21:00', type: 7, name: 'Power Bike'},
        { id: 48, enable: 1, dayofweek: 5, coach: 'Coach 05', hour: '22:00', type: 8, name: 'Zumba'},
        
        { id: 61, enable: 1, dayofweek: 6, coach: 'Coach 01', hour: '09:00', type: 2, name: 'TBC' },
        { id: 62, enable: 1, dayofweek: 6, coach: 'Coach 01', hour: '10:00', type: 3, name: 'Pilates'},
        { id: 63, enable: 1, dayofweek: 6, coach: 'Coach 01', hour: '11:00', type: 4, name: 'Core'},
        { id: 64, enable: 1, dayofweek: 6, coach: 'Coach 02', hour: '12:00', type: 5, name: 'Crossfit'},
        { id: 65, enable: 1, dayofweek: 6, coach: 'Coach 03', hour: '13:30', type: 6, name: 'ABT'},
        { id: 66, enable: 1, dayofweek: 6, coach: 'Coach 04', hour: '14:00', type: 7, name: 'Power Bike'},
        { id: 67, enable: 1, dayofweek: 6, coach: 'Coach 05', hour: '15:00', type: 8, name: 'Zumba'},
        { id: 68, enable: 1, dayofweek: 6, coach: 'Coach 04', hour: '16:00', type: 7, name: 'Power Bike'},
        { id: 69, enable: 1, dayofweek: 6, coach: 'Coach 03', hour: '17:30', type: 9, name: 'Fat Burning'},
        
        { id: 70, enable: 1, dayofweek: 7, coach: 'Coach 01', hour: '09:30', type: 4, name: 'Core'},
        { id: 71, enable: 1, dayofweek: 7, coach: 'Coach 01', hour: '10:00', type: 3, name: 'Pilates'},
        { id: 72, enable: 1, dayofweek: 7, coach: 'Coach 01', hour: '11:00', type: 2, name: 'TBC' },
        { id: 73, enable: 1, dayofweek: 7, coach: 'Coach 04', hour: '12:00', type: 7, name: 'Power Bike'},
        { id: 74, enable: 1, dayofweek: 7, coach: 'Coach 03', hour: '13:30', type: 9, name: 'Fat Burning'},
        { id: 75, enable: 1, dayofweek: 7, coach: 'Coach 02', hour: '14:30', type: 5, name: 'Crossfit'},
    ];

});