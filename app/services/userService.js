managerApp.service('userService', function () {

    this.getUser = function (id) {
        for (var i = 0; i < users.length; i++) {
            if (users[i].id === id) {
                return users[i];
            }
        }
        return null;
    };
    
    this.addReservation = function(id, activity_id){
        var userId = 1;
        for (var i = 0; i < users.length; i++) {
            if(users[i].id === userId){
                var d = new Date();
                var rsvObj = {
                    id: id,
                    activity_id: activity_id,
                    date: d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds(),
                };
                console.log(rsvObj.date);
                users[i].reservations.push(rsvObj);
            }
        }
        return null;
    };
    
    this.cancelReservation = function(id){
        for (var i = 0; i < users.length; i++) {
            for (var j = 0; j < users[i].reservations.length; j++) {
                if(users[i].reservations[j].id === id){
                    console.log(users[i].reservations[j]);
                    users[i].reservations.splice(j,1);
                    return null;
                }
            }
        }
        return null;
    };
    
    var users = [
        { 
            id: 1, first_name: 'Robert', last_name: 'Times',
            reservations: [
                { id: 1, activity_id: 1, date: '2017-10-06 12:10:10' },
                { id: 2, activity_id: 1, date: '2017-10-03 10:10:10' }
            ]
        },
        { 
            id: 2, first_name: 'Ann', last_name: 'Black',
            reservations: [
                { id: 3, activity_id: 1, date: '2017-10-07 12:10:10' },
                { id: 4, activity_id: 1, date: '2017-10-08 10:10:10' }
            ]
        }
    ];

    
});