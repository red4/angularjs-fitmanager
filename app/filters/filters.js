managerApp.filter('dateToISO', function () {
    return function (input) {
        return new Date(input).toISOString();
    };
});