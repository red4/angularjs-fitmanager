managerApp.controller('CalendarController', function ($scope, $location, userService, calendarService) {
    
    $scope.getHours = function(){
        var startHour = 9;
        var endHour   = 22;
        var step  = 30;
        var hours = [];
        for (var i = startHour; i <= endHour; i++) {
            if(i < 10){
                hours.push('0'+i+':00');
                hours.push('0'+i+':'+step);
            } else {
                hours.push(i+':00');
                hours.push(i+':'+step);
            }
        }
        return hours;
    };
    $scope.dayHours = $scope.getHours();
    $scope.userID = 1;
    
    $scope.getReservationNextID = function(){
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
    };
    
    $scope.addReservation = function (activity_id) {
        userService.addReservation($scope.getReservationNextID,activity_id);
        $location.path('/user/'+$scope.userID);
    };
    
    $scope.cancelReservation = function (reservation_id) {
        console.log(reservation_id);
        userService.cancelReservation(reservation_id);
        $scope.activities = $scope.getActivitiesByUser($scope.userID);
        $location.path('/user/'+$scope.userID);
    };    
    
    $scope.getActivitiesByUser = function(userID){
        var activities = calendarService.getActivities();
        var user = userService.getUser(userID);
        for (var i = 0; i < activities.length; i++) {
            for (var j = 0; j < user.reservations.length; j++) {
                if(activities[i].id === user.reservations[j].activity_id){
                    activities[i].enable = 0;
                    activities[i].reservation_id = user.reservations[j].id;
                }
            }
        }
        return activities;
    };
    
    $scope.addEmptyHours = function(activities){
        var weekStart = 1;
        var weekEnd = 7;
        var activitiesWithEmpty = [];
        for (var i = weekStart; i <= weekEnd; i++) {
            for (var j = 0; j < $scope.dayHours.length; j++) {
                var exist = false;
                for (var k = 0; k < activities.length; k++) {
                    if((activities[k].dayofweek === i) 
                        && (activities[k].hour === $scope.dayHours[j])){
                        exist = activities[k];
                    }
                }
                if(exist !== false){
                    activitiesWithEmpty.push(exist);
                } else {
                    activitiesWithEmpty.push({ id: 99999, enable: 0, dayofweek: i, coach: '', hour: $scope.dayHours[j], name: '', type: 1 });
                }
            }
        }
        return activitiesWithEmpty;
    };

    init();

    function init() {
        $scope.activities = $scope.getActivitiesByUser($scope.userID);
        $scope.activitiesEmpty = $scope.addEmptyHours($scope.activities);
    }
});

managerApp.controller('UserController', function ($scope, $routeParams, userService, calendarService ) {
    $scope.getUser = function(ID){
        var user = userService.getUser(ID);

        for (var i = 0; i < user.reservations.length; i++) {
            user.reservations[i].activity 
                    = calendarService.getActivity(user.reservations[i].activity_id);
        }
    
        return user;
    };
    
    $scope.cancelReservation = function (id) {
        console.log(id);
        userService.cancelReservation(id);
    };

    init();

    function init() {
        var userID = ($routeParams.ID) ? parseInt($routeParams.ID) : 0;
        if (userID > 0) {
            $scope.user = $scope.getUser(userID);
        }
        
    }
});