var managerApp = angular.module('managerApp',['ngRoute']);
managerApp.config(function ($routeProvider) {
    $routeProvider
        .when('/',
            {
                controller: 'CalendarController',
                templateUrl: 'app/partials/calendarViewWeek.html'
            })
        .when('/calendar',
            {
                controller: 'CalendarController',
                templateUrl: 'app/partials/calendarViewWeek.html'
            })
        .when('/user/:ID',
            {
                controller: 'UserController',
                templateUrl: 'app/partials/user.html'
            })
        .otherwise({ redirectTo: '/calendar' });
});